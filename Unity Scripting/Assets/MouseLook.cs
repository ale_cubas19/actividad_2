using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public GameObject camerasParent; //parent object of all cameras that should rotate with mouse
    public float hRotationSpeed = 100f; // player rotates along y axis 
    public float vRotationSpeed = 80f;  //cam rotates along x axis
    public float maxVerticalAngle;   // maimun rotation along x axis
    public float minVerticalAngle;  //minimun rotation along x axis
    public float smoothTime = 0.05f;

    float vCamRotationAngles; // variable to apply vertical rotation
    float hPlayerRotation;  // variable to apply horizontal rotation
    float currentHVelocity;  // smooth horizontal velocity
    float currentVVelocity;  // smooth vertical velocity
    float targetCamEulers;  // variable to accumulate the eulers angles along x axis
    Vector3 targetCamRotation;  // aux variable to store the targetrotation of the camerasParent avoiding to instatiate a new vector 3 every frame

    void Start()
    {
        //hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void handleRotation(float hInput, float vInput)
    {
        //GetRotation based on input
        float targetPlayerRotation = hInput * hRotationSpeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationSpeed * Time.deltaTime;

        //Player rotation
        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref currentHVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRotation, 0f);



        //Cam rotation
        targetCamEulers = Mathf.Clamp(targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles, targetCamEulers, ref currentVVelocity, smoothTime);
        targetCamRotation.Set(-vCamRotationAngles, 0f, 0f);
        camerasParent.transform.localEulerAngles = targetCamRotation;
     }
}
