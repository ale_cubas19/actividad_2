using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloWorld : MonoBehaviour
{

    // Variable pública expuesta en el inspector
    public string mensajePersonalizable = "¡Hola, mundo!";

    void Update()
    {
        // Impresión del mensaje en la consola al inicio
        Debug.Log(mensajePersonalizable);
    }
}