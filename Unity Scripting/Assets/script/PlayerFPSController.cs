using System.Diagnostics;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterMovement ))]
[RequireComponent (typeof (MouseLook ))]

public class PlayerFPSController : MonoBehaviour
{
    private CharacterMovement characterMovement;
    private MouseLook mouseLook;
    private readonly float vMovementInput;

    public float hMovementInput { get; private set; }

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();
    }

    void Update()
    {
        Movement();
        Rotation();
    }

    private void Movement()
    {
        //movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void Rotation() { 
    //rotation
    float hRotationInput = Input.GetAxis("Mouse X");
    float vRotationInput = Input.GetAxis("Mouse Y");
  
    mouseLook.handleRotation(hRotationInput, vRotationInput);

    }
}

    